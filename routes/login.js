var express = require('express');
var axios = require('axios');
var router = express.Router();

const loginService = axios.create({
  baseURL: 'https://api.authy.com/',
  headers: {
    'X-Authy-API-Key': 'BGx16lS71UekNQE0ctioETq23Jifr7GG'
  }
});

const NEW_USER = 'protected/json/users/new/';
const REQUEST_CODE = 'protected/json/sms/';
const VERIFY_CODE = 'protected/json/verify/';

router.get('/send-code/:userId', function(req, res, next) {
  console.log(req.params);
  loginService.get(REQUEST_CODE + req.params.userId).then(loginRes => {
    console.log(loginRes.data);
    res.send(loginRes.data);
  });
});

module.exports = router;
